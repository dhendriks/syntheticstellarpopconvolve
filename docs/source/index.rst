.. binary_c-python documentation master file, created by
   sphinx-quickstart on Wed Nov 13 11:43:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of Synthetic Stellar Pop Convolve (SSPC)!
======================================================================
.. mdinclude:: ../../README.md


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme_link
   example_notebooks
   modules
   default_convolution_config
   Visit the GitLab repo <https://gitlab.com/dhendriks/syntheticstellarpopconvolve/>
   Submit an issue <https://gitlab.com/dhendriks/syntheticstellarpopconvolve/-/issues/new>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
