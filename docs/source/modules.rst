SSPC code-base
==============
This section contains the (auto)documentation for all the functions and modules in the source code of `SSPC`.

.. toctree::
   :maxdepth: 4

   modules/check_input_file
   modules/calculate_birth_redshift_array
   modules/check_convolution_config
   modules/default_convolution_config
   modules/general_functions
   modules/metallicity_distributions
   modules/starformation_rate_distributions
   modules/convolve_custom_data
   modules/convolve_ensembles
   modules/convolve_events
   modules/convolve_populations
   modules/convolve
   modules/cosmology_utils
   modules/dicts
   modules/extract_population_settings
   modules/prepare_output_file
   modules/prepare_redshift_interpolator
   modules/SFR_plotting_routine
   modules/store_redshift_shell_info
   modules/store_sfr_info
   modules/update_convolution_config
