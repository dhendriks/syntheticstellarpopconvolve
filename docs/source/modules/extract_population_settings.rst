extract_population_settings module
==================================

.. automodule:: syntheticstellarpopconvolve.extract_population_settings
   :members:
   :undoc-members:
   :show-inheritance:
