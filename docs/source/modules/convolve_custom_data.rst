convolve_custom_data module
===========================

.. automodule:: syntheticstellarpopconvolve.convolve_custom_data
   :members:
   :undoc-members:
   :show-inheritance:
