check_input_file module
=======================

.. automodule:: syntheticstellarpopconvolve.check_input_file
   :members:
   :undoc-members:
   :show-inheritance:
