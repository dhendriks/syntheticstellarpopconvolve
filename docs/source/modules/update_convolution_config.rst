update_convolution_config module
================================

.. automodule:: syntheticstellarpopconvolve.update_convolution_config
   :members:
   :undoc-members:
   :show-inheritance:
