convolve module
===============

.. automodule:: syntheticstellarpopconvolve.convolve
   :members:
   :undoc-members:
   :show-inheritance:
