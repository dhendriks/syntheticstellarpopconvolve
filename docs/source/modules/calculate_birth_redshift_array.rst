calculate_birth_redshift_array module
=====================================

.. automodule:: syntheticstellarpopconvolve.calculate_birth_redshift_array
   :members:
   :undoc-members:
   :show-inheritance:
