starformation_rate_distributions module
======================================

.. automodule:: syntheticstellarpopconvolve.starformation_rate_distributions
   :members:
   :undoc-members:
   :show-inheritance:
