default_convolution_config module
==================================

.. automodule:: syntheticstellarpopconvolve.default_convolution_config
   :members:
   :undoc-members:
   :show-inheritance:
