metallicity_distributions module
================================

.. automodule:: syntheticstellarpopconvolve.metallicity_distributions
   :members:
   :undoc-members:
   :show-inheritance:
