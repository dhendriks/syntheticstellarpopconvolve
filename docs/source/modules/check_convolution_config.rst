check_convolution_config module
===============================

.. automodule:: syntheticstellarpopconvolve.check_convolution_config
   :members:
   :undoc-members:
   :show-inheritance:
