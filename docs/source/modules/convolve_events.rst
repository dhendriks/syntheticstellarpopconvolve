convolve_events module
======================

.. automodule:: syntheticstellarpopconvolve.convolve_events
   :members:
   :undoc-members:
   :show-inheritance:
