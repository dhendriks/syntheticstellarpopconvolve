store_redshift_shell_info module
================================

.. automodule:: syntheticstellarpopconvolve.store_redshift_shell_info
   :members:
   :undoc-members:
   :show-inheritance:
