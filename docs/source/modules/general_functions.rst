general_functions module
========================

.. automodule:: syntheticstellarpopconvolve.general_functions
   :members:
   :undoc-members:
   :show-inheritance:
