convolve_populations module
===========================

.. automodule:: syntheticstellarpopconvolve.convolve_populations
   :members:
   :undoc-members:
   :show-inheritance:
