Example notebooks
=================
We have a set of notebooks that explain and show the usage of the SSPC features. The notebooks are also stored in the `examples directory in the repository <https://gitlab.com/dhendriks/syntheticstellarpopconvolve/-/tree/master/examples>`_

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    examples/notebook_convolution_tutorial.ipynb
    examples/notebook_convolution_advanced.ipynb
    examples/notebook_convolution_star_formation_functions.ipynb

    examples/notebook_convolution_use_cases.ipynb
